provider "google" {
    credentials = "${file("./creds/serviceaccount.json")}"

    project = "${var.google_project}"
    region  = "${var.google_region}"
    zone    = "${var.google_zone}"
}

resource "google_compute_firewall" "default" {
    name    = "desafio-bry-firewall"
    network = "default"

    allow {
        protocol = "tcp"
        ports    = ["80", "8080", "443"]
    }
}

resource "google_dns_managed_zone" "default_zone" {
  name        = "desafio-bry-zone"
  dns_name    = "desafio-bry.tk."
  description = "Example DNS zone"
}

resource "google_dns_record_set" "teste" {
    name = "devops.${google_dns_managed_zone.default_zone.dns_name}"
    type = "A"
    ttl = 300

    managed_zone = "${google_dns_managed_zone.default_zone.name}"

    rrdatas = ["${google_compute_instance.vm.network_interface.0.access_config.0.nat_ip}"]
}

resource "google_compute_instance" "vm" {
    name                      = "vm"
    machine_type              = "n1-standard-2"
    zone                      = "us-central1-b"
    allow_stopping_for_update = true

    boot_disk {
        initialize_params {
            image = "centos-cloud/centos-7"
        }
    }

  metadata = {
   ssh-keys = "${var.os_user}:${var.id_rsa}"
 }

    network_interface {
        network = "default"

        access_config {
             nat_ip = "${google_compute_address.static.address}"

        }
    }
}
resource "google_compute_address" "static" {
    name = "ip-vm"
}
