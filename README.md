# desafio-bry-terraform

## Descrição

Esse repositório é responsável pelo provisionamento de máquinas da [Google Cloud Plataform](https://cloud.google.com/) utilizando a ferramenta [Terraform](https://www.terraform.io/) na versão ``0.12.18``.

Caso seja de seu interesse entender como foi realizada a configuração da máquina utilizada para o desafio via Ansible, favor dirija-se ao repositório [desafio-bry-ansible](https://gitlab.com/roberto.rvs/desafio-bry-ansible).

## Índice
* [Descrição](#descrição)
* [Provisionando máquinas via Terraform na GCP](#Provisionando-máquinas-via-Terraform-na-gcp)
* [Arquivo de variáveis](#arquivo-de-variáveis)
* [Gitlab CI](#gitlab-ci)

## Provisionando máquinas via Terraform na GCP

Quando falamos em automatização da infraestrutura, torna-se difícil não abordarmos a ferramenta Terraform desenvolvida pela [HashiCorp](https://www.hashicorp.com/). Esta ferramenta permite o provisionamento de máquinas em quase todas as nuvens, sendo caracterizada como MultiCloud.

Para o desafio, a Cloud escolhida foi a Google Cloud Platform (GCP), pois ao criar uma conta gratuita são disponibilizados 1000 reais em créditos para testes.

### Arquivo de Configuração do Terraform

No arquivo de configuração [main.tf](https://gitlab.com/roberto.rvs/desafio-bry/blob/master/main.tf), começamos com as informações do provider da GCP:

```hcl
provider "google" {
    credentials = "${file("./creds/serviceaccount.json")}"

    project = "${var.google_project}"
    region  = "${var.google_region}"
    zone    = "${var.google_zone}"
}
```

Por questões de segurança, codifiquei as credenciais de acesso ao projeto na GCP usando o [base64](https://linuxhint.com/bash_base64_encode_decode/) do Linux e transformei `project`, `region` e `zone` em variáveis,  tornando mais fácil e seguro modificar essas informações sem que haja a necessidade de mexer no arquivo principal.

Criamos um firewall para permitirmos apenas que as portas necessárias para o desafio fiquem habilitadas, garantindo que não hajam futuros problemas de vulnerabilidade:

```hcl
resource "google_compute_firewall" "default" {
    name    = "desafio-bry-firewall"
    network = "default"

    allow {
        protocol = "tcp"
        ports    = ["80", "8080", "443"]
    }
}
```

Logo após, provemos na GCP uma [zona gerenciada](https://cloud.google.com/dns/zones/) para o domínio ``desafio-bry.tk`` criado no provedor de domínios gratuitos [Freenom](https://www.freenom.com/pt/aboutfreenom.html).

```hcl
resource "google_dns_managed_zone" "default_zone" {
  name        = "desafio-bry-zone"
  dns_name    = "desafio-bry.tk."
  description = "Example DNS zone"
}
```

Seguindo, criamos o record set ``devops.bry-desafio.tk`` do tipo A para o domínio desafio-bry.tk mostrado anteriormente. Esse record set será utilizado como nosso DNS ao longo do desafio.

```hcl
resource "google_dns_record_set" "teste" {
    name = "devops.${google_dns_managed_zone.default_zone.dns_name}"
    type = "A"
    ttl = 300

    managed_zone = "${google_dns_managed_zone.default_zone.name}"

    rrdatas = ["${google_compute_instance.vm.network_interface.0.access_config.0.nat_ip}"]
}
```
Em ``managed_zone`` referenciamos a zona gerenciada criada anteriormente e em ``rrdatas`` referenciamos o ip estático que será criado em seguida junto às informações da vm que utilizaremos.

Criamos a``vm`` utilizada para o desafio, contendo seu tamanho, disco, interface de rede e seu ip estático.

```hcl
resource "google_compute_instance" "vm" {
    name                      = "vm"
    machine_type              = "n1-standard-2"
    zone                      = "us-central1-b"
    allow_stopping_for_update = true

    boot_disk {
        initialize_params {
            image = "centos-cloud/centos-7"
        }
    }

  metadata = {
   ssh-keys = "${var.os_user}:${var.id_rsa}"
 }

    network_interface {
        network = "default"

        access_config {
             nat_ip = "${google_compute_address.static.address}"

        }
    }
}
```

Em ``metadata``, passamos a chave ssh que será utilizada para rodar o ansible e para acessar a máquina via terminal. Note que por segurança, optei por tornar os dados variáveis que estarão presentes no arquivo [variables.tf](https://gitlab.com/roberto.rvs/desafio-bry/blob/master/variables.tf). Dessa forma não ficam expostos no código.

### Observações
Para que o DNS ``devops.desafio-bry.tk`` passasse a resolver, foi necessário adicionar no Freenom os quatro nameservers cedidos pela GCP após o provisionamento da zona, sendo eles:
* NS-CLOUD-C1.GOOGLEDOMAINS.COM
* NS-CLOUD-C2.GOOGLEDOMAINS.COM
* NS-CLOUD-C3.GOOGLEDOMAINS.COM
* NS-CLOUD-C4.GOOGLEDOMAINS.COM

## Arquivo de variáveis

Optei por deixar algumas informações no arquivo de [variáveis](https://gitlab.com/roberto.rvs/desafio-bry/blob/master/variables.tf) por serem dados que comumente se alteram, em decorrência de cortes de orçamento ou atualização das regiões.

## GitLab CI
Para o CI optei em usar uma imagem minha criada para deployar as ferramentas Ansible e Terraform - ``robertovrs/ansible-terraform`` e dividi os comandos ``validate``, ``plan``, e ``apply`` por estágios, garantindo a validação do arquivo de configuração e o planejamento da execução antes que a aplicação seja rodada na GCP.


Em ``before-scripts`` realizei o decode na base64 das credenciais do projeto e criei um arquivo json que será passado para o provedor no arquivo de configuração do Terraform como citado anteriormente:

```yml
before_script:
  - terraform --version
  - mkdir -p ./creds
  - echo $SERVICE_ACCOUNT | base64 -d > ./creds/serviceaccount.json
  - terraform init
```

O arquivo completo pode ser verificado [aqui](https://gitlab.com/roberto.rvs/desafio-bry/blob/master/.gitlab-ci.yml).
