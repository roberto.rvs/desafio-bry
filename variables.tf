variable "id_rsa" {
    default = "$ID_RSA"  
}

variable "google_project" {
    default = "desafio-bry"
}

variable "google_zone" {
    default = "us-central1-c"
}

variable "google_region" {
    default = "us-central1"
}

variable "os_user" {
    default = "roberto"
}
